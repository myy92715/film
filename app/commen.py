import os
import uuid
import datetime
import stat
from werkzeug.utils import secure_filename
from pypinyin import lazy_pinyin
from flask import current_app


# 修改文件名称
def change_filename(filename):
    fileinfo = os.path.splitext(filename)
    filename = datetime.datetime.now().strftime('%Y%m%d%H%M%S') + str(
        uuid.uuid4().hex + fileinfo[-1])
    return filename


def safe_filename(filename):
    # 用secure_filename获取中文文件名时,获取到的只有后缀？？？
    # 使用汉字转拼音(pypinyin)把中文文件名需要转换成英文
    filename = secure_filename(''.join(lazy_pinyin(filename)))
    return filename


def save_file(file, dirname=None):
    # 保存文件
    # return 文件名
    file_save_path = os.path.join(current_app.config['STATIC_FOLDER'],
                                  dirname)  # 文件上传保存路径
    if not os.path.exists(file_save_path):
        os.makedirs(file_save_path)  # 如果文件保存路径不存在，则创建一个多级目录
        os.chmod(file_save_path, stat.S_IRWXU)  # 授予可读写权限
    filename = change_filename(safe_filename(file.filename))
    file.save(os.path.join(file_save_path, filename))
    return filename


def del_file(filename, dirname=None):
    # 删除上传的文件
    file_save_path = os.path.join(current_app.config['STATIC_FOLDER'],
                                  dirname)  # 文件上传保存路径
    if filename:
        file = os.path.join(file_save_path, filename)
        if os.path.exists(file):
            os.remove(file)
