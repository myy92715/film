from flask_wtf import FlaskForm
from wtforms import (StringField, PasswordField, TextAreaField, FileField,
                     SubmitField)
from wtforms.validators import (InputRequired, ValidationError, EqualTo, Email,
                                Regexp)
from app.models import User
from flask import session


class RegisterForm(FlaskForm):
    '''会员注册'''
    name = StringField(
        label='昵称',
        validators=[InputRequired('请输入昵称！')],
        description='昵称',
        render_kw={
            'class': "form-control input-lg",
            'placeholder': "请输入昵称",
        })
    email = StringField(
        label='邮箱',
        validators=[InputRequired('请输入邮箱！'),
                    Email('邮箱格式不正确')],
        description='邮箱',
        render_kw={
            'class': "form-control input-lg",
            'placeholder': "请输入邮箱",
        })
    phone = StringField(
        label='手机',
        validators=[
            InputRequired('请输入手机！'),
            Regexp('^1[3|4|5|6|7|8][0-9]\d{4,8}$', message='手机格式不正确')
        ],
        description='手机',
        render_kw={
            'class': "form-control input-lg",
            'placeholder': "请输入手机",
        })
    pwd = PasswordField(
        label='密码',
        validators=[InputRequired('请输入密码！')],
        description='密码',
        render_kw={
            'class': "form-control input-lg",
            'placeholder': "请输入密码",
        })
    repwd = PasswordField(
        label='重复密码',
        validators=[
            InputRequired('请输入重复密码！'),
            EqualTo('pwd', message='两次密码不一致')
        ],
        description='重复密码',
        render_kw={
            'class': "form-control input-lg",
            'placeholder': "请输入重复密码",
        })

    def validate_name(self, field):
        name = field.data
        num = User.query.filter_by(name=name).count()
        if num == 1:
            raise ValidationError('昵称已经存在，请重新输入')

    def validate_email(self, field):
        email = field.data
        num = User.query.filter_by(email=email).count()
        if num == 1:
            raise ValidationError('邮箱已经存在，请重新输入')

    def validate_phone(self, field):
        phone = field.data
        num = User.query.filter_by(phone=phone).count()
        if num == 1:
            raise ValidationError('手机号已经存在，请重新输入')


class LoginFrom(FlaskForm):
    """会员登录表单"""
    name = StringField(
        label='账号',
        validators=[InputRequired('请输入账号！')],
        description='账号',
        render_kw={
            'class': "form-control input-lg",
            'placeholder': "请输入账号",
        })

    pwd = PasswordField(
        label='密码',
        validators=[InputRequired('请输入密码！')],
        description='密码',
        render_kw={
            'class': "form-control input-lg",
            'placeholder': "请输入密码",
        })

    def validate_name(self, field):
        """从Admin数据库中，检测账号是否存在，如果不存在则在account.errors中添加错误信息"""
        account = field.data
        num = User.query.filter_by(name=account).count()
        if num == 0:
            raise ValidationError('账号不存在')


class UserDetailForm(FlaskForm):
    name = StringField(
        label='昵称',
        validators=[InputRequired('请输入昵称！')],
        description='昵称',
        render_kw={
            'class': "form-control",
            'placeholder': "请输入昵称",
        })
    email = StringField(
        label='邮箱',
        validators=[InputRequired('请输入邮箱！'),
                    Email('邮箱格式不正确')],
        description='邮箱',
        render_kw={
            'class': "form-control",
            'placeholder': "请输入邮箱",
        })
    phone = StringField(
        label='手机',
        validators=[
            InputRequired('请输入手机！'),
            Regexp('^1[3|4|5|6|7|8][0-9]\d{4,8}$', message='手机格式不正确')
        ],
        description='手机',
        render_kw={
            'class': "form-control",
            'placeholder': "请输入手机",
        })
    face = FileField(
        label='头像',
        validators=[InputRequired('请上传头像')],
        description='头像',
        render_kw={'class': "form-control"})
    info = TextAreaField(
        label='简介',
        validators=[],
        description='简介',
        render_kw={
            'class': "form-control",
            'rows': "10",
        })


class PwdForm(FlaskForm):
    '''修改密码'''
    old_pwd = PasswordField(
        label='旧密码',
        validators=[InputRequired('请输入旧密码！')],
        description='旧密码',
        render_kw={
            'class': "form-control",
            'placeholder': "请输入旧密码",
        })
    new_pwd = PasswordField(
        label='新密码',
        validators=[InputRequired('请输入新密码！')],
        description='新密码',
        render_kw={
            'class': "form-control",
            'placeholder': "请输入新密码",
        })
    new_pwd_rep = PasswordField(
        label='确认密码',
        validators=[
            InputRequired('请再次输入新密码确认！'),
            EqualTo('new_pwd', message='两次密码不一致')
        ],
        description='再次输入新密码',
        render_kw={
            'class': "form-control",
            'placeholder': "再次输入新密码",
        })

    def validate_old_pwd(self, field):
        user = User.query.filter_by(name=session['login_user']).first()
        if not user.check_pwd(field.data):
            raise ValidationError('旧密码错误！')


class CommentForm(FlaskForm):
    content = TextAreaField(
        label='内容',
        validators=[InputRequired('请输入内容')],
        description='内容',
        render_kw={
            'class': "form-control",
            'rows': "5"
        })
    submit = SubmitField(
        label='提交评论', render_kw={
            "class": "btn btn-success",
            "id": "btn-sub"
        })
