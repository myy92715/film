import time
import json
import uuid
import datetime
from functools import wraps
from sqlalchemy import extract
from app.home import home
from app import db, fr
from flask import (render_template, redirect, url_for, flash, request, session,
                   Response)
from app.home.forms import (RegisterForm, LoginFrom, UserDetailForm, PwdForm,
                            CommentForm)
from app.models import User, Userlog, Comment, Moviecol, Preview, Movie, Tag
from app.commen import save_file, del_file


# 要求登录才能访问
def user_login_require(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if session.get('login_user', None) is None:
            # 如果session中未找到该键，则用户需要登录
            return redirect(url_for('home.login', next=request.path))
        return func(*args, **kwargs)

    return decorated_function


@home.route("/", methods=['GET'])
@home.route("/<int:page>/", methods=['GET'])
def index(page=None):
    page = 1 if page is None else page
    all_tag = Tag.query.all()
    # 星级转换
    star_list = [(1, '1星'), (2, '2星'), (3, '3星'), (4, '4星'), (5, '5星')]
    all_star = map(lambda x: {'num': x[0], 'info': x[1]}, star_list)
    # 年份列表
    now_year = time.localtime()[0]
    year_range = [year for year in range(int(now_year), int(now_year) - 5, -1)]
    # print(year_range)
    page_movies = Movie.query
    selected = dict()
    tag_id = request.args.get('tag_id', 0)  # 获取链接中的标签id，0为显示所有
    if int(tag_id) != 0:
        page_movies = page_movies.filter_by(tag_id=tag_id)
    selected['tag_id'] = tag_id

    star_num = request.args.get('star_num', 0)  # 获取星级数字，0为显示所有
    if int(star_num) != 0:
        page_movies = page_movies.filter_by(star=star_num)
    selected['star_num'] = int(star_num)

    time_year = request.args.get('time_year', 1)  # 1为所有日期，0为更早，月份为所选
    if int(time_year) == 0:
        page_movies = page_movies.filter(
            extract('year', Movie.release_time) < int(now_year) - 5)
    elif int(time_year) == 1:
        page_movies = page_movies  # 所有年份的电影
    else:
        page_movies = page_movies.filter(
            extract('year', Movie.release_time) == time_year)  # 筛选年份
    selected['time_year'] = time_year

    playnum = request.args.get('playnum', 1)  # 1为从高到低，0为从低到好
    if int(playnum) == 1:
        page_movies = page_movies.order_by(Movie.playnum.desc())
    else:
        page_movies = page_movies.order_by(Movie.playnum.asc())
    selected['playnum'] = playnum

    commentnum = request.args.get('commentnum', 1)  # 1为从高到低，0为从低到好
    if int(commentnum) == 1:
        page_movies = page_movies.order_by(Movie.commentnum.desc())
    else:
        page_movies = page_movies.order_by(Movie.commentnum.asc())
    selected['commentnum'] = commentnum

    page_movies = page_movies.paginate(page=page, per_page=12)
    return render_template(
        'home/index.html',
        all_tag=all_tag,
        all_star=all_star,
        now_year=now_year,
        year_range=year_range,
        selected=selected,
        page_movies=page_movies)


@home.route('/login/', methods=['GET', 'POST'])
def login():
    form = LoginFrom()
    if form.validate_on_submit():
        data = form.data
        user = User.query.filter_by(name=data['name']).first()
        if not user.check_pwd(data['pwd']):
            flash('密码错误', category='error')
            return redirect(url_for('home.login'))
        session['login_user'] = user.name
        session['login_user_id'] = user.id
        userlog = Userlog(user_id=user.id, ip=request.remote_addr)
        db.session.add(userlog)
        db.session.commit()
        return redirect(url_for('home.user'))
    return render_template('home/login.html', form=form)


@home.route('/logout/')
def logout():
    session.pop('login_user', None)
    session.pop('login_user_id', None)
    return redirect(url_for('home.login'))


@home.route('/register/', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        data = form.data
        user = User(
            name=data['name'],
            email=data['email'],
            phone=data['phone'],
            uuid=uuid.uuid4().hex)
        user.set_pwd(form.pwd.data)
        db.session.add(user)
        db.session.commit()
        flash('注册成功', category='success')
        return redirect(url_for('home.login'))
    return render_template('home/register.html', form=form)


@home.route('/user/', methods=['GET', 'POST'])
@user_login_require
def user():
    login_user = User.query.get_or_404(int(session['login_user_id']))
    form = UserDetailForm(
        name=login_user.name,
        email=login_user.email,
        phone=login_user.phone,
        info=login_user.info)
    form.face.validators = []
    form.face.flags.required = False
    if form.validate_on_submit():
        # 上传文件
        if form.face.data:
            del_file(login_user.face, 'user')
            login_user.face = save_file(form.face.data, 'user')

        if login_user.name != form.name.data and User.query.filter_by(
                name=form.name.data).count() > 0:
            flash('昵称已经存在', 'error')
            return redirect(url_for('home.user'))
        login_user.name = form.name.data

        if login_user.email != form.email.data and User.query.filter_by(
                email=form.email.data).count() > 0:
            flash('昵称已经存在', 'error')
            return redirect(url_for('home.user'))
        login_user.email = form.email.data

        if login_user.phone != form.phone.data and User.query.filter_by(
                phone=form.phone.data).count() > 0:
            flash('昵称已经存在', 'error')
            return redirect(url_for('home.user'))
        login_user.phone = form.phone.data

        login_user.info = form.info.data
        db.session.commit()
        flash('修改资料成功', 'success')
        return redirect(url_for('home.user'))
    return render_template('home/user.html', form=form, login_user=login_user)


@home.route('/pwd/', methods=['GET', 'POST'])
@user_login_require
def pwd():
    form = PwdForm()
    if form.validate_on_submit():
        user = User.query.filter_by(name=session['login_user']).first_or_404()
        user.set_pwd(form.new_pwd.data)
        db.session.commit()
        flash('密码修改成功，请重新登录', category='success')
        return redirect(url_for('home.logout'))
    return render_template('home/pwd.html', form=form)


@home.route("/comments/", methods=['GET'])
@home.route("/comments/<int:page>/", methods=['GET'])
@user_login_require
def comments(page=None):
    page = 1 if page is None else page
    page_comments = Comment.query.filter_by(
        user_id=int(session['login_user_id'])).order_by(
            Comment.addtime.desc()).paginate(
                page=page, per_page=10)
    return render_template('home/comments.html', page_comments=page_comments)


@home.route("/userlog/", methods=['GET'])
@home.route("/userlog/<int:page>/", methods=['GET'])
@user_login_require
def userlog(page=None):
    page = 1 if page is None else page
    page_userlog = Userlog.query.filter_by(
        user_id=int(session['login_user_id'])).order_by(
            Userlog.addtime.desc()).paginate(
                page=page, per_page=10)
    return render_template('home/userlog.html', page_userlog=page_userlog)


@home.route("/moviecols/", methods=['GET'])
@home.route("/moviecols/<int:page>/", methods=['GET'])
@user_login_require
def moviecols(page=None):
    page = 1 if page is None else page
    page_moviecols = Moviecol.query.filter_by(
        user_id=int(session['login_user_id'])).order_by(
            Moviecol.addtime.desc()).paginate(
                page=page, per_page=10)
    return render_template(
        'home/moviecols.html', page_moviecols=page_moviecols)


@home.route('/moviecols/add/', methods=['GET', 'POST'])
@user_login_require
def add_moviecols():
    movie_id = request.args.get('movie_id', '')
    user_id = request.args.get('user_id', '')
    movie_collect = Moviecol.query.filter_by(
        user_id=int(user_id), movie_id=int(movie_id)).count()
    if movie_collect > 0:
        return json.dumps({'ok': 0})
    movie_collect = Moviecol(user_id=int(user_id), movie_id=int(movie_id))
    db.session.add(movie_collect)
    db.session.commit()
    return json.dumps({'ok': 0})


@home.route('/indexbanner/')
def indexbanner():
    previews = Preview.query.all()
    return render_template('home/indexbanner.html', previews=previews)


@home.route('/search/')
def search():
    keyword = request.args.get('keyword')
    search_movies = Movie.query.filter(
        Movie.title.ilike("%" + keyword + "%")).order_by(Movie.addtime.desc())
    search_count = Movie.query.filter(
        Movie.title.ilike("%" + keyword + "%")).count()
    return render_template(
        'home/search.html',
        keyword=keyword,
        search_movies=search_movies,
        search_count=search_count)


@home.route('/play/<int:movie_id>/', methods=['GET', 'POST'])
@home.route('/play/<int:movie_id>/page/<int:page>/', methods=['GET', 'POST'])
def play(movie_id, page=None):
    movie = Movie.query.join(Tag).filter(
        Tag.id == Movie.tag_id, Movie.id == int(movie_id)).first_or_404()
    if request.method == 'GET':
        movie.playnum += 1
        db.session.commit()
    form = CommentForm()
    if 'login_user' not in session:
        form.submit.render_kw = {
            'disabled': "disabled",
            "class": "btn btn-success",
            "id": "btn-sub"
        }
    if form.validate_on_submit() and 'login_user' in session:
        data = form.data
        comment = Comment(
            content=data['content'],
            movie_id=movie.id,
            user_id=session['login_user_id'])
        db.session.add(comment)
        movie.commentnum += 1
        db.session.commit()
        flash('评论成功', category='success')
        return redirect(url_for('home.play', movie_id=movie.id))
    page = 1 if page is None else page
    page_comments = Comment.query.join(Movie).join(User).filter(
        Movie.id == movie.id,
        User.id == Comment.user_id).order_by(Comment.addtime.desc()).paginate(
            page=page, per_page=10)
    return render_template(
        'home/play.html', movie=movie, form=form, page_comments=page_comments)


@home.route('/tm/v3/', methods=['GET', 'POST'])
def tm():
    resp = ''
    if request.method == 'GET':  # 获取弹幕
        movie_id = request.args.get('id')
        key = 'movie{}:barrage'.format(movie_id)
        if fr.llen(key):
            msgs = fr.lrange(key, 0, 2999)
            tm_data = []
            for msg in msgs:
                msg = json.loads(msg)
                tmp_data = [
                    msg['time'], msg['type'], msg['date'], msg['author'],
                    msg['text']
                ]
                tm_data.append(tmp_data)
            res = {
                'code': 0,
                'data': tm_data,
            }
        else:
            print('Redis中暂无内容')
            res = {'code': 1, 'data': []}
        resp = json.dumps(res)
    if request.method == "POST":  # 添加弹幕
        data = json.loads(request.get_data())
        # print(data)
        msg = {
            "__v": 0,
            "author": data["author"],
            "time": data["time"],  # 发送弹幕视频播放进度时间
            "date": int(time.time()),  # 当前时间戳
            "text": data["text"],  # 弹幕内容
            "color": data["color"],  # 弹幕颜色
            "type": data['type'],  # 弹幕位置
            "ip": request.remote_addr,
            "_id": datetime.datetime.now().strftime("%Y%m%d%H%M%S") + uuid.uuid4().hex,
            "player": data['id'],
        }
        res = {"code": 0, "data": msg}
        resp = json.dumps(res)
        # 将添加的弹幕推入redis的队列中
        fr.lpush("movie{}:barrage".format(data['id']), json.dumps(msg))
    return Response(resp, mimetype='application/json')
