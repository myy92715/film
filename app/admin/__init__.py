from flask import Blueprint

admin = Blueprint('admin', __name__)

import app.admin.views
import app.admin.tag_view
import app.admin.movie_view
import app.admin.priview_view
import app.admin.user_view
import app.admin.comment_view
import app.admin.collect_view
import app.admin.auth_view
import app.admin.role_view
