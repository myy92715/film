'''
权限管理视图
'''
from app.admin import admin
from flask import render_template, redirect, url_for, flash
from app.admin.forms import AuthForm
from app.models import Auth
from app import db
from app.admin.views import admin_login_require, permission_control
from sqlalchemy import or_


@admin.route("/auth/add/", methods=['GET', 'POST'])
@admin_login_require
@permission_control
def auth_add():
    form = AuthForm()
    if form.validate_on_submit():
        data = form.data
        auth_num = Auth.query.filter(
            or_(Auth.name == data['name'], Auth.url == data['url'])).count()
        if auth_num > 0:
            flash('权限已存在！', category='error')
            return redirect(url_for('admin.auth_add'))

        # 如果标签不存在，就添加到数据库
        auth = Auth(name=data['name'], url=data['url'])
        db.session.add(auth)
        db.session.commit()
        # 提交完成后也返回一条成功的消息
        flash('权限地址添加成功！', category='success')
        return redirect(url_for('admin.auth_add'))
    return render_template('admin/auth_add.html', form=form)


@admin.route("/auth/list/", methods=['GET'])
@admin.route("/auth/list/<int:page>/", methods=['GET'])
@admin_login_require
@permission_control
def auth_list(page=None):
    page = 1 if page is None else page
    # 设置per_page每页显示多少条数据
    page_auths = Auth.query.order_by(Auth.addtime.desc()).paginate(
        page=page, per_page=15)
    return render_template('admin/auth_list.html', page_auths=page_auths)


@admin.route('/auth/delete/<int:delete_id>/', methods=['GET'])
@admin_login_require
@permission_control
def auth_delete(delete_id):
    if delete_id:
        auth = Auth.query.get_or_404(delete_id)
        db.session.delete(auth)
        db.session.commit()
        flash('删除权限地址成功', category='success')
    return redirect(url_for('admin.auth_list'))


@admin.route("/auth/update/<int:update_id>/", methods=['GET', 'POST'])
@admin_login_require
@permission_control
def auth_update(update_id):
    auth = Auth.query.get_or_404(update_id)
    form = AuthForm(name=auth.name, url=auth.url)
    if form.validate_on_submit():
        data = form.data
        auth_num = Auth.query.filter_by(url=data['url']).count()
        if auth_num > 0 and auth.url != data['url']:
            flash('权限链接地址已存在！', category='error')
            return redirect(url_for('admin.auth_update', update_id=update_id))

        # 如果标签不存在，就添加到数据库
        auth.name = data['name']
        auth.url = data['url']
        db.session.commit()
        # 提交完成后也返回一条成功的消息
        flash('权限地址修改成功！', category='success')
        return redirect(url_for('admin.auth_update', update_id=update_id))
    return render_template('admin/auth_add.html', form=form, auth=auth)
