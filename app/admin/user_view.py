'''
会员视图
'''
from app import db
from app.admin import admin
from app.models import User
from app.admin.views import admin_login_require, permission_control
from flask import render_template, redirect, url_for, flash
from app.commen import del_file


@admin.route("/user/list/", methods=['GET'])
@admin.route("/user/list/<int:page>/", methods=['GET'])
@admin_login_require
@permission_control
def user_list(page=None):
    page = 1 if page is None else page
    page_users = User.query.order_by(User.id.desc()).paginate(
        page=page, per_page=10)
    return render_template('admin/user_list.html', page_users=page_users)


@admin.route("/user/detail/<int:user_id>/")
@admin_login_require
@permission_control
def user_detail(user_id):
    user = User.query.get_or_404(user_id)
    return render_template('admin/user_detail.html', user=user)


@admin.route("/user/delete/<int:delete_id>/")
@admin_login_require
@permission_control
def user_delete(delete_id):
    user = User.query.get_or_404(delete_id)
    del_file(user.face, 'user')
    db.session.delete(user)
    db.session.commit()
    flash('删除会员成功！', category='success')
    return redirect(url_for('admin.user_list'))
