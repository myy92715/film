'''
角色管理视图
'''
from app.admin import admin
from flask import render_template, redirect, url_for, flash
from app.admin.forms import RoleForm
from app.models import Role
from app import db
from app.admin.views import admin_login_require, permission_control


@admin.route("/role/add/", methods=['GET', 'POST'])
@admin_login_require
@permission_control
def role_add():
    form = RoleForm()
    if form.validate_on_submit():
        data = form.data
        role_num = Role.query.filter_by(name=data['name']).count()
        if role_num > 0:
            flash('角色已存在！', category='error')
            return redirect(url_for('admin.role_add'))

        # 如果角色不存在，就添加到数据库
        role = Role(
            name=data['name'],
            auths=','.join(map(lambda item: str(item),
                               data['auths'])))  # 数字转换为字符串形式)
        db.session.add(role)
        db.session.commit()
        # 提交完成后也返回一条成功的消息
        flash('角色添加成功！', category='success')
        return redirect(url_for('admin.role_add'))
    return render_template('admin/role_add.html', form=form)


@admin.route("/role/list/", methods=['GET'])
@admin.route("/role/list/<int:page>/", methods=['GET'])
@admin_login_require
@permission_control
def role_list(page=None):
    page = 1 if page is None else page
    # 设置per_page每页显示多少条数据
    page_roles = Role.query.order_by(Role.addtime.desc()).paginate(
        page=page, per_page=10)
    return render_template('admin/role_list.html', page_roles=page_roles)


@admin.route('/role/delete/<int:delete_id>/', methods=['GET'])
@admin_login_require
@permission_control
def role_delete(delete_id):
    if delete_id:
        role = Role.query.get_or_404(delete_id)
        db.session.delete(role)
        db.session.commit()
        flash('删除角色成功', category='success')
    return redirect(url_for('admin.role_list'))


@admin.route("/role/update/<int:update_id>/", methods=['GET', 'POST'])
@admin_login_require
@permission_control
def role_update(update_id):
    role = Role.query.get_or_404(update_id)
    form = RoleForm(
        name=role.name,
        auths=list(map(lambda item: int(item), role.auths.split(',')))
        if role.auths else ''  # 换回int型列表
    )
    if form.validate_on_submit():
        data = form.data
        role_num = Role.query.filter_by(name=data['name']).count()
        if role_num > 0 and role.name != data['name']:
            flash('角色已存在！', category='error')
            return redirect(url_for('admin.role_update', update_id=update_id))

        # 如果标签不存在，就添加到数据库
        role.name = data['name']
        role.auths = ','.join(map(lambda item: str(item), data['auths']))
        db.session.commit()
        # 提交完成后也返回一条成功的消息
        flash('角色修改成功！', category='success')
        return redirect(url_for('admin.role_update', update_id=update_id))
    return render_template('admin/role_add.html', form=form, role=role)
