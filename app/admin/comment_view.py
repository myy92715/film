'''
评论视图
'''
from app import db
from app.admin import admin
from flask import render_template, redirect, url_for, flash
from app.models import Comment, Movie, User
from app.admin.views import admin_login_require, permission_control


@admin.route("/comment/list/", methods=['GET'])
@admin.route("/comment/list/<int:page>/", methods=['GET'])
@permission_control
def comment_list(page=None):
    page = 1 if page is None else page
    page_comments = Comment.query.join(Movie).join(User).filter(
        Movie.id == Comment.movie_id,
        User.id == Comment.user_id).order_by(Comment.addtime.desc()).paginate(
            page=page, per_page=10)
    return render_template(
        'admin/comment_list.html', page_comments=page_comments)


@admin.route("/comment/delete/<int:delete_id>/")
@admin_login_require
@permission_control
def comment_delete(delete_id):
    comment = Comment.query.get_or_404(delete_id)
    db.session.delete(comment)
    db.session.commit()
    flash('删除评论成功！', category='success')
    return redirect(url_for('admin.comment_list'))
