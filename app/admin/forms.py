from flask_wtf import FlaskForm
from wtforms import (StringField, PasswordField, SubmitField, FileField,
                     SelectField, TextAreaField, SelectMultipleField)
from wtforms.validators import InputRequired, ValidationError, EqualTo
from app.models import Admin, Tag, Auth, Role
from flask import session


class LoginFrom(FlaskForm):
    '''管理员登录表单'''
    account = StringField(
        label='账号',
        validators=[InputRequired('请输入账号！')],
        description='账号',
        render_kw={
            'class': 'form-control',
            'placeholder': '请输入账号',
            'required': 'required'
        })
    pwd = PasswordField(
        label='密码',
        validators=[InputRequired('请输入密码！')],
        description='密码',
        render_kw={
            'class': 'form-control',
            'placeholder': '请输入密码',
            'required': 'required'
        })
    submit = SubmitField(
        label='登录', render_kw={'class': "btn btn-primary btn-block btn-flat"})

    def validate_account(self, field):
        admin_num = Admin.query.filter_by(name=field.data).count()
        if admin_num == 0:
            raise ValidationError('账号不存在')


class TagForm(FlaskForm):
    '''添加电影标签'''
    name = StringField(
        label='名称',
        validators=[InputRequired('标签名称不能为空！')],
        description='标签',
        render_kw={
            'class': "form-control",
            'placeholder': "请输入标签名称！"
        })
    submit = SubmitField(label='提交', render_kw={'class': "btn btn-primary"})


class MovieForm(FlaskForm):
    '''添加电影'''
    title = StringField(
        label='片名',
        validators=[InputRequired('请输入片名！')],
        description='片名',
        render_kw={
            'class': "form-control",
            'placeholder': "请输入标签名称！"
        })
    url = FileField(
        label='电影文件',
        validators=[InputRequired('请上传电影文件！')],
        description='电影文件',
        render_kw={'class': "form-control"})
    info = TextAreaField(
        label='简介',
        validators=[InputRequired('请输入简介！')],
        description='简介',
        render_kw={
            'class': "form-control",
            'rows': "10",
        })
    logo = FileField(
        label='封面',
        validators=[InputRequired('请上传封面！')],
        description='封面',
        render_kw={'class': "form-control"})
    star = SelectField(
        label='星级',
        validators=[InputRequired('请选择星级！')],
        description='星级',
        coerce=int,
        choices=[(1, '1星'), (2, '2星'), (3, '3星'), (4, '4星'), (5, '5星')],
        render_kw={'class': "form-control"})
    tag_id = SelectField(
        label='标签',
        validators=[InputRequired('请选择标签！')],
        coerce=int,
        choices='',
        description='标签',
        render_kw={'class': "form-control"})
    area = StringField(
        label='上映地区',
        validators=[InputRequired('请输入上映地区！')],
        description='上映地区',
        render_kw={
            'class': "form-control",
            'placeholder': "请输入上映地区！"
        })
    length = StringField(
        label='播放时长(分钟)',
        validators=[InputRequired('请输入播放时长！')],
        description='播放时长',
        render_kw={
            'class': "form-control",
            'placeholder': "请输入播放时长！",
        })
    release_time = StringField(
        label='上映时间',
        validators=[InputRequired('请选择上映时间！')],
        description='上映时间',
        render_kw={
            'class': "form-control",
            'placeholder': "请选择上映时间！",
        })

    def __init__(self, *args, **kwargs):  # 这里的self是一个RegisterForm对象
        '''重写__init__方法'''
        super(MovieForm, self).__init__(*args, **kwargs)  # 继承父类的init方法
        self.tag_id.choices = [(tag.id, tag.name) for tag in Tag.query.all()]


class PreviewForm(FlaskForm):
    '''添加电影预告'''
    title = StringField(
        label='预告标题',
        validators=[InputRequired('请输入预告标题！')],
        description='请输入预告标题！',
        render_kw={'class': 'form-control'})
    logo = FileField(
        label='预告封面',
        validators=[InputRequired('请上传预告封面！')],
        render_kw={'class': "form-control"})


class PwdForm(FlaskForm):
    '''修改密码'''
    old_pwd = PasswordField(
        label='旧密码',
        validators=[InputRequired('请输入旧密码！')],
        description='旧密码',
        render_kw={
            'class': "form-control",
            'placeholder': "请输入旧密码",
        })
    new_pwd = PasswordField(
        label='新密码',
        validators=[InputRequired('请输入新密码！')],
        description='新密码',
        render_kw={
            'class': "form-control",
            'placeholder': "请输入新密码",
        })
    new_pwd_rep = PasswordField(
        label='确认密码',
        validators=[
            InputRequired('请再次输入新密码确认！'),
            EqualTo('new_pwd', message='两次密码不一致')
        ],
        description='再次输入新密码',
        render_kw={
            'class': "form-control",
            'placeholder': "再次输入新密码",
        })

    def validate_old_pwd(self, field):
        admin = Admin.query.filter_by(name=session['login_admin']).first()
        if not admin.check_pwd(field.data):
            raise ValidationError('旧密码错误！')


class AuthForm(FlaskForm):
    '''权限表单'''
    name = StringField(
        label='权限名称',
        validators=[InputRequired('请输入权限名称！')],
        description='请输入权限名称！',
        render_kw={'class': 'form-control'})
    url = StringField(
        label='访问链接',
        validators=[InputRequired()],
        description='请输入访问链接！',
        render_kw={
            'class': 'form-control',
            'placeholder': '链接地址'
        })


class RoleForm(FlaskForm):
    '''角色表单'''
    name = StringField(
        label='角色名称',
        validators=[InputRequired('请输入角色名称！')],
        description='请输入角色名称！',
        render_kw={'class': 'form-control'})
    auths = SelectMultipleField(
        label='权限列表',
        coerce=int,
        choices=[],
        description='请选择权限！',
        render_kw={
            'class': 'form-control',
            'style': 'height: 200px'
        })

    def __init__(self, *args, **kwargs):  # 这里的self是一个RegisterForm对象
        '''重写__init__方法'''
        super(RoleForm, self).__init__(*args, **kwargs)  # 继承父类的init方法
        self.auths.choices = [(auth.id, auth.name)
                              for auth in Auth.query.all()]


class AdminForm(FlaskForm):
    '''管理员表单'''
    name = StringField(
        label='管理员名称',
        validators=[InputRequired('请输入管理员名称！')],
        description='管理员名称',
        render_kw={
            'class': 'form-control',
            'placeholder': '请输入管理员名称'
        })

    pwd = PasswordField(
        label='管理员密码',
        validators=[InputRequired('请输入管理员密码！')],
        description='管理员密码',
        render_kw={
            'class': "form-control",
            'placeholder': "请输入管理员密码",
        })
    repwd = PasswordField(
        label='管理员重复密码',
        validators=[
            InputRequired('请输入管理员重复密码！'),
            EqualTo('pwd', message='两次密码不一致')
        ],
        description='管理员重复密码',
        render_kw={
            'class': "form-control",
            'placeholder': "请输入管理员重复密码",
        })
    is_super = SelectField(
        label='星级',
        validators=[InputRequired('请选择星级！')],
        description='星级',
        coerce=int,
        choices=[(1, '普通管理员'), (0, '超级管理员')],
        render_kw={'class': "form-control"})
    role_id = SelectField(
        label='所属角色',
        validators=[InputRequired('请选择所属角色！')],
        coerce=int,
        # choices=[(role.id, role.name) for role in Role.query.all()],
        description='所属角色',
        render_kw={'class': "form-control"})

    def __init__(self, *args, **kwargs):
        super(AdminForm, self).__init__(*args, **kwargs)
        self.role_id.choices = [(v.id, v.name) for v in Role.query.all()]
