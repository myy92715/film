'''
电影视图
'''
from app import db
from app.admin import admin
from flask import render_template, redirect, url_for, flash
from app.admin.forms import MovieForm
from app.models import Tag, Movie
from app.commen import save_file, del_file
from app.admin.views import admin_login_require, permission_control


@admin.route("/movie/add/", methods=['GET', 'POST'])
@admin_login_require
@permission_control
def movie_add():
    form = MovieForm()
    if form.validate_on_submit():
        data = form.data

        # 提交的片名在数据库中已存在
        if Movie.query.filter_by(title=data['title']).count() > 0:
            flash('电影片名已经存在！', category='error')
            return redirect(url_for('admin.movie_add'))
        # 保存文件
        url_filename = save_file(form.url.data, 'movie')
        logo_filename = save_file(form.logo.data, 'movie')
        # 入库
        movie = Movie(
            title=data['title'],
            url=url_filename,
            info=data['info'],
            logo=logo_filename,
            star=data['star'],
            playnum=0,
            commentnum=0,
            tag_id=data['tag_id'],
            area=data['area'],
            release_time=data['release_time'],
            length=data['length'])
        db.session.add(movie)
        db.session.commit()
        flash('添加电影成功', category='success')
        return redirect(url_for('admin.movie_add'))
    return render_template('admin/movie_add.html', form=form)


@admin.route("/movie/list/", methods=['GET'])
@admin.route("/movie/list/<int:page>/", methods=['GET'])
@permission_control
def movie_list(page=None):
    page = 1 if page is None else page
    # 查询的时候关联标签Tag进行查询：使用join(Tag)
    # 单表过滤使用filter_by，多表关联使用filter，将Tag.id与Movie的tag_id进行关联
    page_movies = Movie.query.join(Tag).filter(
        Tag.id == Movie.tag_id).order_by(Movie.addtime.desc()).paginate(
            page=page, per_page=10)
    return render_template('admin/movie_list.html', page_movies=page_movies)


@admin.route('/movie/delete/<int:delete_id>/', methods=['GET'])
@admin_login_require
@permission_control
def movie_delete(delete_id=None):
    if delete_id:
        movie = Movie.query.get_or_404(delete_id)
        del_file(movie.url, 'movie')
        del_file(movie.logo, 'movie')
        db.session.delete(movie)
        db.session.commit()
        flash('删除电影成功！', category='success')
    return redirect(url_for('admin.movie_list'))


@admin.route("/movie/update/<int:update_id>/", methods=['GET', 'POST'])
@admin_login_require
@permission_control
def movie_update(update_id=None):
    movie = Movie.query.get_or_404(update_id)
    form = MovieForm(
        title=movie.title,
        info=movie.info,
        star=movie.star,
        tag_id=movie.tag_id,
        area=movie.area,
        release_time=movie.release_time,
        length=movie.length,
    )
    # 取消文件验证
    form.url.validators = []
    form.logo.validators = []
    form.url.flags.required = False
    form.logo.flags.required = False

    if form.validate_on_submit():
        data = form.data
        movie_num = Tag.query.filter_by(name=data['title']).count()
        if movie_num > 0 and movie.title != data['title']:
            flash('电影片名已经存在！', category='error')
            return redirect(url_for('admin.movie_update', update_id=update_id))

        # 修改数据
        movie.title = data['title']
        movie.info = data['info']
        movie.star = data['star']
        movie.tag_id = data['tag_id']
        movie.area = data['area']
        movie.release_time = data['release_time']
        movie.length = data['length']

        # 上传文件
        if data['url']:
            del_file(movie.url, 'movie')
            movie.url = save_file(data['url'], 'movie')
        if data['logo']:
            del_file(movie.logo, 'movie')
            movie.logo = save_file(data['logo'], 'movie')
        db.session.commit()
        # 提交完成后也返回一条成功的消息
        flash('电影修改成功！', category='success')
        return redirect(url_for('admin.movie_update', update_id=update_id))
    return render_template('admin/movie_update.html', form=form, movie=movie)
