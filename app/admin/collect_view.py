'''
收藏视图
'''
from app import db
from app.admin import admin
from flask import render_template, redirect, url_for, flash
from app.models import Moviecol, Movie, User
from app.admin.views import admin_login_require, permission_control


@admin.route("/collect/list/", methods=['GET'])
@admin.route("/collect/list/<int:page>/", methods=['GET'])
@permission_control
def collect_list(page=None):
    page = 1 if page is None else page
    page_collects = Moviecol.query.join(Movie).join(User).filter(
        Movie.id == Moviecol.movie_id, User.id == Moviecol.user_id).order_by(
            Moviecol.addtime.desc()).paginate(
                page=page, per_page=10)
    return render_template(
        'admin/collect_list.html', page_collects=page_collects)


@admin.route("/collect/delete/<int:delete_id>/")
@admin_login_require
@permission_control
def collect_delete(delete_id):
    collect = Moviecol.query.get_or_404(delete_id)
    db.session.delete(collect)
    db.session.commit()
    flash('删除收藏成功！', category='success')
    return redirect(url_for('admin.collect_list'))
