from app.admin import admin
from flask import (render_template, redirect, url_for, flash, request, session,
                   abort)
from app.admin.forms import LoginFrom, PwdForm, AdminForm
from app.models import Admin, Role, Auth
from functools import wraps
from app import db
from wtforms.validators import EqualTo


def admin_login_require(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if session.get('login_admin', None) is None:
            # 如果session中未找到该键，则用户需要登录
            return redirect(url_for('admin.login', next=request.path))
        return func(*args, **kwargs)

    return decorated_function


def permission_control(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        login_admin = Admin.query.join(Role).filter(
            Role.id == Admin.role_id,
            Admin.name == session['login_admin']).first()
        all_auth = Auth.query.all()

        auths = login_admin.role.auths
        auths = list(map(lambda item: int(item), auths.split(',')))
        urls = [
            auth.url for auth in all_auth for admin_auth_id in auths
            if admin_auth_id == auth.id
        ]
        if str(request.url_rule) not in urls and login_admin.is_super != 0:
            abort(401)
        return func(*args, **kwargs)

    return decorated_function


@admin.route('/')
@admin_login_require
def index():
    return render_template('admin/index.html')


@admin.route('/login/', methods=['GET', 'POST'])
def login():
    form = LoginFrom()
    if form.validate_on_submit():
        data = form.data
        login_admin = Admin.query.filter_by(name=data['account']).first()
        if not login_admin.check_pwd(data['pwd']):
            flash('密码错误！', category='error')
            return redirect(url_for('admin.login'))
        session['login_admin'] = data['account']
        return redirect(request.args.get('next') or url_for('admin.index'))
    return render_template('admin/login.html', form=form)


@admin.route('/logout/')
def logout():
    session.pop('login_admin', None)
    return redirect(url_for('admin.login'))


@admin.route('/pwd/', methods=['GET', 'POST'])
@admin_login_require
def pwd():
    form = PwdForm()
    if form.validate_on_submit():
        admin = Admin.query.filter_by(
            name=session['login_admin']).first_or_404()
        admin.set_pwd(form.data['new_pwd'])
        db.session.commit()
        flash('密码修改成功，请重新登录', category='success')
        return redirect(url_for('admin.logout'))
    return render_template('admin/pwd.html', form=form)


@admin.route("/admin/add/", methods=['GET', 'POST'])
@admin_login_require
@permission_control
def admin_add():
    form = AdminForm(is_super=1)
    if form.validate_on_submit():
        admin_num = Admin.query.filter_by(name=form.name.data).count()
        if admin_num > 0:
            flash('管理员已存在！', category='error')
            return redirect(url_for('admin.admin_add'))
        admin = Admin(
            name=form.name.data, role_id=form.role_id.data, is_super=1)
        admin.set_pwd(form.pwd.data)
        db.session.add(admin)
        db.session.commit()
        flash('管理员添加成功', category='success')
    return render_template('admin/admin_add.html', form=form)


@admin.route("/admin/list/", methods=['GET'])
@admin.route("/admin/list/<int:page>/", methods=['GET'])
@admin_login_require
@permission_control
def admin_list(page=None):
    page = 1 if page is None else page
    # 设置per_page每页显示多少条数据
    page_admins = Admin.query.order_by(Admin.addtime.desc()).join(Role).filter(
        Role.id == Admin.role_id).paginate(
            page=page, per_page=10)
    return render_template('admin/admin_list.html', page_admins=page_admins)


@admin.route('/admin/delete/<int:delete_id>/', methods=['GET'])
@admin_login_require
@permission_control
def admin_delete(delete_id):
    if delete_id:
        admin = Admin.query.get_or_404(delete_id)
        db.session.delete(admin)
        db.session.commit()
        flash('删除管理员成功', category='success')
    return redirect(url_for('admin.admin_list'))


@admin.route("/admin/update/<int:update_id>/", methods=['GET', 'POST'])
@admin_login_require
@permission_control
def admin_update(update_id):
    admin = Admin.query.get_or_404(update_id)
    form = AdminForm(
        name=admin.name, role_id=admin.role_id, is_super=admin.is_super)
    form.pwd.validators = []
    form.repwd.validators = [EqualTo('pwd', message='两次密码不一致')]
    form.pwd.flags.required = False
    form.repwd.flags.required = False
    if form.validate_on_submit():
        data = form.data
        admin_num = Admin.query.filter_by(name=data['name']).count()
        if admin_num > 0 and admin.name != data['name']:
            flash('管理员已存在！', category='error')
            return redirect(url_for('admin.admin_update', update_id=update_id))

        # 如果标签不存在，就添加到数据库
        admin.name = data['name']
        admin.role_id = data['role_id']
        admin.is_super = data['is_super']
        if form.pwd.data and form.repwd.data:
            admin.set_pwd(form.pwd.data)
        db.session.commit()
        # 提交完成后也返回一条成功的消息
        flash('管理员修改成功！', category='success')
        return redirect(url_for('admin.admin_update', update_id=update_id))
    return render_template('admin/admin_add.html', form=form, admin=admin)


@admin.route("/logs/operate_log/")
def logs_operate_log():
    return render_template('admin/logs_operate_log.html')


@admin.route("/logs/admin_log/")
def logs_admin_log():
    return render_template('admin/logs_admin_log.html')


@admin.route("/logs/user_log/")
def logs_user_log():
    return render_template('admin/logs_user_log.html')
