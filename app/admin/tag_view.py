'''
电影标签视图
'''
from app.admin import admin
from flask import render_template, redirect, url_for, flash
from app.admin.forms import TagForm
from app.models import Tag
from app import db
from app.admin.views import admin_login_require, permission_control


@admin.route("/tag/add/", methods=['GET', 'POST'])
@admin_login_require
@permission_control
def tag_add():
    form = TagForm()
    if form.validate_on_submit():
        data = form.data
        tag_num = Tag.query.filter_by(name=data['name']).count()
        if tag_num > 0:
            flash('标签名称已存在！', category='error')
            return redirect(url_for('admin.tag_add'))

        # 如果标签不存在，就添加到数据库
        tag = Tag(name=data['name'])
        db.session.add(tag)
        db.session.commit()
        # 提交完成后也返回一条成功的消息
        flash('标签添加成功！', category='success')
        return redirect(url_for('admin.tag_add'))
    return render_template('admin/tag_add.html', form=form)


@admin.route("/tag/list/", methods=['GET'])
@admin.route("/tag/list/<int:page>/", methods=['GET'])
@admin_login_require
@permission_control
def tag_list(page=None):
    page = 1 if page is None else page
    # 设置per_page每页显示多少条数据
    page_tags = Tag.query.order_by(Tag.addtime.desc()).paginate(
        page=page, per_page=10)
    return render_template('admin/tag_list.html', page_tags=page_tags)


@admin.route('/tag/delete/<int:delete_id>/', methods=['GET'])
@admin_login_require
@permission_control
def tag_delete(delete_id=None):
    if delete_id:
        tag = Tag.query.filter_by(id=delete_id).first_or_404()
        db.session.delete(tag)
        db.session.commit()
        flash('删除标签成功！', category='success')
    return redirect(url_for('admin.tag_list'))


@admin.route("/tag/update/<int:update_id>/", methods=['GET', 'POST'])
@admin_login_require
@permission_control
def tag_update(update_id=None):
    form = TagForm()
    tag = Tag.query.get_or_404(update_id)
    if form.validate_on_submit():
        data = form.data
        tag_num = Tag.query.filter_by(name=data['name']).count()
        if tag_num > 0:
            flash('标签名称已存在！', category='error')
            return redirect(url_for('admin.tag_update', update_id=update_id))

        # 如果标签不存在，就添加到数据库
        tag.name = data['name']
        db.session.commit()
        # 提交完成后也返回一条成功的消息
        flash('标签修改成功！', category='success')
        return redirect(url_for('admin.tag_update', update_id=update_id))
    return render_template('admin/tag_update.html', form=form, tag=tag)
