'''
电影预告视图
'''
from flask import flash, redirect, url_for, render_template
from app import db
from app.admin import admin
from app.admin.forms import PreviewForm
from app.models import Preview
from app.admin.views import admin_login_require, permission_control
from app.commen import save_file, del_file


@admin.route('/preview/add/', methods=['GET', 'POST'])
@admin_login_require
@permission_control
def preview_add():
    form = PreviewForm()
    if form.validate_on_submit():
        data = form.data
        if Preview.query.filter_by(title=data['title']).count() > 0:
            flash('预告标题已存在，请检查！', category='error')
            return redirect(url_for('admin.preview_add'))
        logo = save_file(data['logo'], 'preview')

        preview = Preview(title=data['title'], logo=logo)
        db.session.add(preview)
        db.session.commit()
        flash('添加预告成功', 'success')
        return redirect(url_for('admin.preview_add'))
    return render_template('admin/preview_add.html', form=form)


@admin.route("/preview/list/", methods=['GET'])
@admin.route("/preview/list/<int:page>/", methods=['GET'])
@permission_control
def preview_list(page=None):
    page = 1 if page is None else page
    page_previews = Preview.query.order_by(Preview.addtime.desc()).paginate(
        page=page, per_page=10)
    return render_template(
        'admin/preview_list.html', page_previews=page_previews)


@admin.route('/preview/delete/<int:delete_id>/', methods=['GET'])
@admin_login_require
@permission_control
def preview_delete(delete_id=None):
    if delete_id:
        preview = Preview.query.get_or_404(delete_id)
        del_file(preview.logo, 'preview')
        db.session.delete(preview)
        db.session.commit()
        flash('删除预告成功！', category='success')
    return redirect(url_for('admin.preview_list'))


@admin.route("/preview/update/<int:update_id>/", methods=['GET', 'POST'])
@admin_login_require
@permission_control
def preview_update(update_id=None):
    preview = Preview.query.get_or_404(update_id)
    form = PreviewForm(title=preview.title)
    # 取消文件验证
    form.logo.validators = []
    form.logo.flags.required = False

    if form.validate_on_submit():
        data = form.data
        preview_num = Preview.query.filter_by(title=data['title']).count()
        if preview_num > 0 and preview.title != data['title']:
            flash('预告标题已经存在！', category='error')
            return redirect(
                url_for('admin.preview_update', update_id=update_id))

        # 修改数据
        preview.title = data['title']

        # 上传文件
        if data['logo']:
            del_file(preview.logo, 'preview')
            preview.logo = save_file(data['logo'], 'preview')
        db.session.commit()
        # 提交完成后也返回一条成功的消息
        flash('电影预告修改成功！', category='success')
        return redirect(url_for('admin.preview_update', update_id=update_id))
    return render_template(
        'admin/preview_add.html', form=form, preview=preview)
