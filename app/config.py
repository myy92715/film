import os


class BaseConfig(object):
    '''基础配置'''
    # 配置flask配置对象中键：SQLALCHEMY_DATABASE_URI
    SQLALCHEMY_DATABASE_URI = "{dbtype}+{dbdriver}://{dbuser}:{dbpass}@{dbhost}:{dbport}/{dbname}".format(
        dbtype='mysql',
        dbdriver='mysqlconnector',
        dbuser='root',
        dbpass='root',
        dbhost='localhost',
        dbport=3306,
        dbname='movie')
    # 配置flask配置对象中键：SQLALCHEMY_COMMIT_TEARDOWN,设置为True,应用会自动在每次请求结束后提交数据库中变动
    SQLALCHEMY_COMMIT_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True


class DevelopmentConfig(BaseConfig):
    '''开发配置'''
    ENV = 'development'
    DEBUG = True
    SECRET_KEY = 'you-will-never-guess'
    STATIC_FOLDER = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), 'static')
    REDIS_URL = 'redis://:{password}@{host}:{port}/{db}'.format(
        password=123456,
        host='localhost',
        port=6379,
        db=2,
    )


class ProductionConfig(BaseConfig):
    '''生产配置'''
    SECRET_KEY = 'you-will-never-guess'
    STATIC_FOLDER = os.path.join(
        os.path.dirname(os.path.abspath(__file__)), 'static')


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
}
